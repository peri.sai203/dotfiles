local wezterm = require("wezterm")
local act = wezterm.action
local astrotheme = require("astrotheme")

local function font_with_fallback(name, params)
	local names = { name, "Symbols Nerd Font Mono" }
	return wezterm.font_with_fallback(names, params)
end

-- A helper function for my fallback fonts
return {
	-- font = wezterm.font({
	-- 	-- family = "MonoLisa Nerd Font",
	-- 	family = "MonoLisaNerdFont Nerd Font",
	-- 	font_with_fallback = { "Symbols Nerd Font Mono" },
	-- 	-- family = "Liga SFMono Nerd Font",
	-- 	-- bold = "Medium",
	-- 	weight = "Regular",
	-- 	harfbuzz_features = { "zero" },
	-- }),
	font = font_with_fallback({
		family = "MonoLisaNerdFont Nerd Font",
		weight = "Regular",
		harfbuzz_features = { "zero" },
	}),
	front_end = "WebGpu",
	font_size = 13,
	colors = astrotheme.colors(),
	window_background_opacity = 0.95,
	scrollback_lines = 5000,
	term = "xterm-256color",
	-- Window decorations
	hide_tab_bar_if_only_one_tab = true,
	use_fancy_tab_bar = true,
	window_padding = {
		left = 0,
		right = 0,
		-- top = 0,
		-- bottom = 0,
	},
	-- Keybindings
	--

	keys = {
		-- Split vertically
		{
			key = "v",
			mods = "CTRL|SHIFT",
			action = act.SplitHorizontal({ domain = "CurrentPaneDomain" }),
		},

		-- Split horizontal
		{
			key = "s",
			mods = "CTRL|SHIFT",
			action = act.SplitVertical({ domain = "CurrentPaneDomain" }),
		},

		-- Jump to bottom pane
		{
			key = "k",
			mods = "CTRL",
			action = act.ActivatePaneDirection("Up"),
		},
		{
			key = "j",
			mods = "CTRL",
			action = act.ActivatePaneDirection("Down"),
		},
		{ key = "UpArrow", mods = "SHIFT", action = act.ScrollByLine(-1) },

		-- Scroll down 1 line
		{ key = "DownArrow", mods = "SHIFT", action = act.ScrollByLine(1) },
		{ key = "Tab", mods = "ALT|SHIFT", action = act.ActivateTabRelative(-1) },
		{ key = "Tab", mods = "ALT", action = act.ActivateTabRelative(1) },
	},
	-- Shell
	--
	-- MOUSE BINDINGS
	mouse_bindings = {
		-- Paste with right click
		{
			event = {
				Up = {
					streak = 1,
					button = "Right",
				},
			},
			mods = "NONE",
			action = act.PasteFrom("PrimarySelection"),
		},
	},
	-- make clickable hyperlinks work
	hyperlink_rules = {

		-- Linkify things that look like URLs and the host has a TLD name.
		-- Compiled-in default. Used if you don't specify any hyperlink_rules.
		{
			regex = "\\b\\w+://[\\w.-]+\\.[a-z]{2,15}\\S*\\b",
			format = "$0",
		},

		-- linkify email addresses
		-- Compiled-in default. Used if you don't specify any hyperlink_rules.
		{
			regex = [[\b\w+@[\w-]+(\.[\w-]+)+\b]],
			format = "mailto:$0",
		},

		-- file:// URI
		-- Compiled-in default. Used if you don't specify any hyperlink_rules.
		{
			regex = [[\bfile://\S*\b]],
			format = "$0",
		},

		-- Linkify things that look like URLs with numeric addresses as hosts.
		-- E.g. http://127.0.0.1:8000 for a local development server,
		-- or http://192.168.1.1 for the web interface of many routers.
		{
			regex = [[\b\w+://(?:[\d]{1,3}\.){3}[\d]{1,3}\S*\b]],
			format = "$0",
		},
	},
}
